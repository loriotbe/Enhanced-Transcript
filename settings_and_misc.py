import pickle
import types
import os


def get_public_attributes_names(obj, include_methods_name=False):
    """
    Return the list of public attributes' names of a given object.
    Doesn't return public methods' names by default.

    :param obj: the object to use
    :param include_methods_name : If True, include the methods names in the list.
    :return: a list of string containing the attributes' names.
    """
    condition_on_methods = lambda name: include_methods_name or not (isinstance(getattr(obj, name), types.MethodType))

    return [name for name in dir(obj) if name[0] != "_" and condition_on_methods(name)]


def print_object_public_attributes(obj):
    for attr in get_public_attributes_names(obj):
        print("%s = %r" % (attr, getattr(obj, attr)))


def build_object(obj):
    """
    Incrementally modifies an object asking the user to modify
    the objects attributes or not.

    :param obj: an object
    :return: the same (modified) object
    """
    attr_names = get_public_attributes_names(obj)
    for attr_name in attr_names:
        default_value = getattr(obj, attr_name)
        pretty_attr_name = attr_name.replace("_", " ").capitalize()
        new_value = input(f"{pretty_attr_name} (current value : {default_value}) : ")
        if new_value != "":
            setattr(obj, attr_name, new_value)
    return obj


def get_db_string(forcelocal=False):
    """
    Returns the url of the database to use based on the configuration.

    If the URL is not present in the configuration, a local SQLite database
    will be used.

    :param forcelocal: If True, will return the database strign of the local SQLite database.
    :return:
    """
    db_string = "sqlite:///" + database_name
    if forcelocal:
        print("Using local database (forced)")
        return db_string
    try:
        file = open(".credentials", "r")
        content = file.readline()
        file.close()
        parsed = content.replace("\"", "").replace("\n", "")
        if len(parsed) == 0:
            print("Using local SQLite database : " + db_string)
        else:
            db_string = parsed
            print("Using remote database")

    except FileNotFoundError:
        print("Using local SQLite database : " + db_string)
    return db_string


class StudentDetails:
    """
    A wrapper class for the student info.

    Such an object will be persisted using pickle in the `data_folder`
    and will be used to format the final main.tex using latex_dump.
    """

    def __init__(self):
        self.surname = "Dupont"
        self.name = "Jean-Michel"
        self.given_names = "Jean-Michel Luc Charles"
        self.birthday = "10/03/1940"
        self.place_birth = "Pimpousse les Oies"
        self.student_number = "1337"

        self.university = "UTC de Compiègne"
        self.city = "Compiègne"
        self.country = "Pluiecardie"
        self.diploma = "PhD in Collage de Gommette"
        self.diploma_short = "PhD CdG"
        self.major = "Chill avancé"
        self.minor = "Option Club Med"
        self.enrolled_since = "September 2014"
        self.study_end = "July 2019"

    def latex_dump(self):
        return [latex_new_value(name.replace("_", ""), getattr(self, name)) for name in
                get_public_attributes_names(self)]


def latex_new_value(name, value):
    return "\\newcommand{\\" + name + "}{" + value + "}"


def selection_option(driver, drop_down, to_find):
    """
    Home-made function for selenium driver.

    Clicks on the first element of a `drop_down` menu
    containing the text `to_find`.

    If such a text isn't present, it does nothing.

    :param driver: the webdriver to use
    :param drop_down: the drop down menu
    :param to_find: the text to find
    """
    el = driver.find_element_by_id(drop_down)
    for option in el.find_elements_by_tag_name('option'):
        if option.text == to_find:
            option.click()
            break


def select_by_tag(driver, tag, text):
    """
    Home-made function for selenium driver.

    Returns the first element of a specific `tag`
    containing the `text`.

    :param driver: the webdriver to use
    :param tag: the tag of the element
    :param text: the text to find
    """
    elems = driver.find_elements_by_tag_name(tag)
    elems_text = list(map(lambda x: x.text, elems))
    index = elems_text.index(text)

    return elems[index]


def select_by_class_name(driver, class_name, text):
    """
    Home-made function for selenium driver.

    Returns the first element of a specific `class_name`
    containing the `text`.

    :param driver: the webdriver to use
    :param class_name: the class name of the element
    :param text: the text to find
    """
    elems = driver.find_elements_by_class_name(class_name)
    elems_text = list(map(lambda x: x.text, elems))
    index = elems_text.index(text)

    return elems[index]


# Just a new line used for the LaTeXer
bl = " \n "

# URL to scrap the information about results and courses
ent_url = "https://webapplis.utc.fr/ent/index.jsf"
courses_url = "https://webapplis.utc.fr/uvs/index.xhtml"

# Cached data for personnal results and courses information and descriptions
data_folder = "data/"  # not sync with .git by default

student_details_file = os.path.join(data_folder, 'student_details.pickle')
serialized_courses_indexes = os.path.join(data_folder + "courses_indexes.pickle")

# The html file containing the results of the student
results_file = os.path.join(data_folder, 'results.html')

# The local data base persisting the data
database_name = os.path.join(data_folder, 'homeMadeTranscript.db')

# The default lang to use, "en" and "fr" available
lang = "en"
supported_langs = ["fr", "en"]

# The .tex source used to generate the final main doc
out_folder = "out/"
preamble = os.path.join(out_folder, "preamble.tex")
beg_doc = os.path.join(out_folder, "begdoc.tex")
end_doc = os.path.join(out_folder, "enddoc.tex")

# The output file
main_doc = os.path.join(out_folder, "main.tex")


def get_student_details():
    """
    Returns the student details.

    If it doesn't exist, builds it incrementally.

    :return: the persisted StudentDetails.
    """
    try:
        student_details = pickle.load(open(student_details_file, "rb"))
    except FileNotFoundError:

        print("Student details not present : creating them")
        student_details = build_object(StudentDetails())

        pickle.dump(student_details, open(student_details_file, "wb"))

    return student_details