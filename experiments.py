import requests

from models import CourseDescription


def translate_missing_info(session_maker, source="fr", target="en", key_file=".keys"):
    """
    Experimental : Translates the missing curriculum for course from the `source` language to the `target` language.

    Uses the Yandex API for the translation.
    Translations will not be made if the curriculum in the source language doesn't exist.

    :param session_maker: a SessionMaker object (sqlalchemy)
    :param source: the language of the existing translation
    :param target: the language for the translation
    :param key_file: the file contained the key for the API
    """

    # NOTE — about `list(filter(.,.)`\bellow : query.filter(.) should work using several conditions
    # TODO : resolve this using query.filter(. and .) insted
    session = session_maker()
    missing_transl = session.query(CourseDescription) \
        .filter(CourseDescription.curriculum == "").all()

    missing_transl = list(filter(lambda cr: cr.lang == target, missing_transl))
    missing_transl = sorted(missing_transl, key=lambda cd: cd.course_code)
    associated_codes = list(map(lambda course_desc: course_desc.course_code, missing_transl))

    with_transl = session.query(CourseDescription) \
        .filter(CourseDescription.course_code.in_(associated_codes)).all()

    with_transl = list(filter(lambda cr: cr.lang == source and cr.curriculum != "", with_transl))

    with_transl = sorted(with_transl, key=lambda cd: cd.course_code)

    code_to_missing = dict(zip(associated_codes, missing_transl))

    # Translating using Yandex
    url = "https://translate.yandex.net/api/v1.5/tr.json/translate"

    with open(key_file, "r") as file:
        content = file.readline()

    # Fix some fuzzy formatting
    key = content.replace("\"", "").replace("\n", "")

    if key == "":
        raise Exception(f"Key missing in the file {key_file}")

    for to_translate in with_transl:

        data = {
            "text": to_translate.curriculum,
            "format": "plain",
            "lang": f"{source}-{target}",
            "key": key
        }

        res = requests.post(url, data)
        try:
            translation = res.json()["text"][0]
        except KeyError:
            print(res.status_code)
            print(res.content)
        print(f"> {to_translate.course_code}")
        print(translation)
        print()
        code_to_missing[to_translate.course_code].curriculum = translation

    session.commit()
