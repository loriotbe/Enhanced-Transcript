import getpass
import codecs
import re
import time

from collections import deque
from selenium import webdriver
from selenium.common.exceptions import ElementNotVisibleException
from selenium.webdriver.common.keys import Keys
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from bs4 import BeautifulSoup

from settings_and_misc import *
from models import CourseDescription, Course, CourseResult, Semester, reset_db


def export_to_file(driver, filename, encoding="utf8"):
    """
    Exports HTML to file and returns it as a string

    :param driver: the webdriver on the page
    :param filename: filename to use
    :param encoding: encoding to use
    :return: the html as a string
    """
    html = driver.page_source
    file_object = codecs.open(filename, "w", encoding)
    file_object.write(html)
    file_object.close()

    return html


def get_html_results(encoding="utf8"):
    """
    Returns the html file of the student's results.

    Saves it if not present.

    :return: an string of the html
    """
    html = ""
    try:
        with open(results_file, "r", encoding=encoding) as file:
            html = file.read()

    except FileNotFoundError:
        print("Results not present, getting them now !")
        print("You will be asked to enter your credentials in this console.")
        try:
            driver = webdriver.Chrome()
            driver.get(ent_url)

            # Connecting using the CAS
            if "CAS" in driver.title:
                print("Please enter your credentials")
                login = input("Login : ")
                password = getpass.getpass()

                input_login = driver.find_element_by_name("username")
                input_pswd = driver.find_element_by_name("password")
                input_login.clear()
                input_pswd.clear()
                input_login.send_keys(login)
                input_pswd.send_keys(password)
                input_pswd.send_keys(Keys.RETURN)
                assert "MAUVAIS IDENTIFIANT" not in driver.page_source

            select_by_class_name(driver, "col-sm-4", "MON DOSSIER ÉTUDIANT").click()

            # Focus on the new opened tab
            driver.switch_to.window(driver.window_handles[1])

            # Switching between frames
            driver.switch_to.frame(driver.find_element_by_name("menu"))
            driver.find_element_by_id("domaine_3").click()
            driver.switch_to.default_content()
            driver.switch_to.frame(driver.find_element_by_name("frame_contenu"))

            # Saving the results in html
            print(f"Saving results in {results_file} !")
            html = export_to_file(driver, results_file, encoding="iso-8859-15")
            driver.close()

        except Exception as e:
            print(e)

    return html


def get_text(driver, xpath):
    """
    Returns the text of a HTML element based using a xpath.

    :param driver: the driver to use
    :param xpath: the xpath of the element
    :return: the text as a string
    """
    return driver.find_element_by_xpath(xpath).text.strip()


def parse_course(driver, lang):
    """
    Return the Course and CourseDescription of a specific course
    in the specific language given using a driver set
    on the course description webpage.

    :param driver: the webdriver to use
    :param lang: the language of the description
    :return: the associated Course, CourseDescription objects
    """
    if lang not in supported_langs:
        raise ValueError(f"lang should be in {supported_langs} ; actual value : {lang}")

    pause = 0.5

    # "Informations Principales" Menu
    driver.find_element_by_xpath('//*[@id="myTab"]/li[1]/a').click()
    time.sleep(pause)

    header = get_text(driver, '//*[@id="Detail"]/div/div/div[1]/h4')
    print(f"> {header}")
    course_code = "" if len(header.split()) == 0 else header.split()[0]
    suffix_pattern = '[A-Z]{2,3}\d{2} :(?=)'
    to_remove = re.search(suffix_pattern, header)
    if to_remove:
        to_remove = to_remove.group()
    else:
        to_remove = ""

    title = header.replace(to_remove, "").strip().capitalize()
    ects = int(get_text(driver, '//*[@id="accueil"]/table/tbody/tr[1]/td[2]').replace(" ECTS", ""))
    type_ = get_text(driver, '//*[@id="accueil"]/table/tbody/tr[4]/td[2]')

    if type_ == "Scientifiques (CS-ST)":
        type_ = "CS"
    if type_ == "Techniques (TM-ST)":
        type_ = "TM"
    if type_ == "Technologie et Sciences de l'Homme":
        type_ = "TSH"

    teaching_semesters = get_text(driver, '//*[@id="accueil"]/table/tbody/tr[5]/td[2]')

    taught_in_fall = 'Automne' in teaching_semesters
    taught_in_spring = 'Printemps' in teaching_semesters

    # "Description" Menu
    driver.find_element_by_xpath('//*[@id="myTab"]/li[2]/a').click()
    time.sleep(pause)
    overview = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[1]/td[2]')
    bibliography = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[2]/td[2]')
    recommended_level = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[3]/td[2]')
    has_final_exam = 'Oui' in get_text(driver, '//*[@id="menu1"]/table/tbody/tr[4]/td[2]')
    assessment_criteria = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[5]/td[2]')
    success_criteria = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[6]/td[2]')
    misc = get_text(driver, '//*[@id="menu1"]/table/tbody/tr[7]/td[2]')

    # "Profil" Menu
    # driver.find_element_by_xpath('//*[@id="myTab"]/li[3]/a').click()

    # "Objectifs" Menu
    driver.find_element_by_xpath('//*[@id="myTab"]/li[4]/a').click()
    time.sleep(pause)
    training_objectives = ""  # get_text(driver,'//*[@id="menu3"]/table/tbody/tr[1]/td[2]/')
    pedagogical_objectives = get_text(driver, '//*[@id="menu3"]/table/tbody/tr[2]/td[2]')
    other_objectives = get_text(driver, '//*[@id="menu3"]/table/tbody/tr[3]/td[2]')

    # "Informations complémentaires" Menu
    driver.find_element_by_xpath('//*[@id="myTab"]/li[5]/a').click()
    time.sleep(pause)
    curriculum = get_text(driver, '//*[@id="menu4"]/table/tbody/tr[1]/td[2]')
    outcomes = get_text(driver, '//*[@id="menu4"]/table/tbody/tr[2]/td[2]')

    course = Course(course_code=course_code,
                    ects=ects,
                    type=type_,
                    taught_in_spring=taught_in_spring,
                    taught_in_fall=taught_in_fall,
                    has_final_exam=has_final_exam
                    )

    course_description = CourseDescription(
        course_code=course_code,
        lang=lang,
        title=title,
        overview=overview,
        bibliography=bibliography,
        recommended_level=recommended_level,
        assessment_criteria=assessment_criteria,
        success_criteria=success_criteria,
        misc=misc,
        training_objectives=training_objectives,
        pedagogical_objectives=pedagogical_objectives,
        other_objectives=other_objectives,
        curriculum=curriculum,
        outcomes=outcomes)

    return course, course_description


def scrap_courses_description(lang, courses_to_scrap=None):
    """
    Returns Course and CourseDescription from the UTC website.

    :param lang: the lang to use to scrap : "fr" or "en"
    :param courses_to_scrap: a list of course_code indicating the course are to scrap.
    If None, all the courses and descriptions will be scrapped.
    :return: courses : a list of Course,
    courses_descriptions : a list of CourseDescription,
    missing_courses_code : a list of course_code of the courses not available on the website
    """
    driver = webdriver.Chrome()
    driver.get(courses_url)
    driver.maximize_window()
    close_overlay_xpath = '//*[@id="Detail"]/div/div/div[1]/button/span'

    def xpath_of_course_code(number):
        return f'//*[@id="mainForm"]/div/table/tbody/tr[{number+2}]/th[1]'

    def xpath_of_description(number):
        return f'//*[@id="mainForm:dtUvs:{number}:j_idt241"]/span'

    # Switching language
    if lang == "fr":
        driver.find_element_by_xpath('//*[@id="mainForm:j_idt180"]/img').click()
    else:
        driver.find_element_by_xpath('//*[@id="mainForm:j_idt186"]/img').click()
    time.sleep(2)

    diploma_option = "Ingénieur UTC - Branche" if lang == "fr" \
        else "Specialized engineering studies diploma"

    drop_down = "mainForm:sel_diplomes" if lang == "fr" \
        else "mainForm:sel_diplomesen"

    # Engineering degree & Humanities
    selection_option(driver, drop_down, diploma_option)
    time.sleep(3)

    def get_courses_index(driver):
        """
        Returns the index for locating course on the webpage

        :param driver: the webdriver to use
        :return: a dict like {"UV00" : 42, ... }
        """

        try:
            courses_index = pickle.load(open(serialized_courses_indexes, "rb"))
            return courses_index
        except FileNotFoundError:
            print("> Extracting courses indexes")
            course_to_parse = True
            index = 0
            courses_index = dict()

            while course_to_parse:
                elems = driver.find_elements_by_xpath(xpath_of_course_code(index))

                if len(elems) == 0:
                    course_to_parse = False
                    print(f"Done extracting {len(courses_index)} courses indexes !")
                else:
                    course_code = elems[0].text
                    courses_index[course_code] = index

                index += 1

            pickle.dump(courses_index, open(serialized_courses_indexes, "wb"))
            return courses_index

    courses_index = get_courses_index(driver)

    if courses_to_scrap is None:
        courses_to_scrap = courses_index.keys()

    missing_courses_code = set(courses_to_scrap).difference(courses_index.keys())
    if len(missing_courses_code) != 0:
        print(f"Following courses aren't present on the site {missing_courses_code}")
        courses_to_scrap = list(set(courses_to_scrap).intersection(courses_index.keys()))

    # Going through the list, extracting Courses and CourseDecriptions
    courses = []
    courses_descriptions = []

    # We need to pause between click sometimes
    pause = 1

    # To resolved problems related to scrolling on the page
    courses_to_scrap = sorted(courses_to_scrap)

    for course_code in courses_to_scrap:
        print(f"Scrapping {course_code}")
        xpath = xpath_of_description(courses_index[course_code])

        # Opening the course overlay
        driver.find_element_by_xpath(xpath).click()
        try:
            time.sleep(pause)
            course, course_description = parse_course(driver, lang)
            courses.append(course)
            courses_descriptions.append(course_description)

        except ElementNotVisibleException as e:
            print(e)
            print(f" !> Element not visible for {course_code}! Not saving anything for this course.")

        # Closing the course overlay
        time.sleep(pause)
        driver.find_element_by_xpath(close_overlay_xpath).click()
        time.sleep(pause)

    return courses, courses_descriptions, missing_courses_code


def scrap_results(html_result):
    """
    Scraps the student Semesters and CourseResults from the html string passed.

    :param html_result: the html as a string
    :return: semesters : list of Semesters, course_results : list of CourseResult
    """
    soup = BeautifulSoup(html_result, 'html.parser')

    # Getting the wtf html table
    course_table = soup.find_all('table')[1]

    # Fixtures to parse this wtf html table
    def get_in_tree(html_elem, indexes_in_tree):
        indexes_deque = deque(indexes_in_tree)
        if len(indexes_deque) == 0:
            return html_elem
        else:
            index = indexes_deque.popleft()
            return get_in_tree(html_elem.contents[index], indexes_deque)

    get_html_semester = lambda index: get_in_tree(course_table, [0, 2 * index + 2])

    type_course = {"CS": 2, "TM": 4, "TSH": 6, "ST": 8}

    html_course_of_semester = lambda type, sem, course_sindex: get_in_tree(sem,
                                                                          [type_course[type], 1, 1, 2 * course_index])

    # List to be returned
    semesters = []
    course_results = []

    def course_result_from_html(html_course, date_semester):
        course_code = html_course.contents[1].get_text()

        if "TX" in course_code or "PR" in course_code:
            course_code = course_code[0:3] + course_code[-1]

        result = html_course.contents[2].get_text()

        return CourseResult(course_code=course_code, date_semester=date_semester, result=result)

    def semester_from_html(html_sem):
        date_semester = get_in_tree(html_sem, [0, 0, 0, 0]).get_text()
        level_semester = get_in_tree(html_sem, [0, 0, 0, 1]).get_text()
        observation = get_in_tree(html_sem, [10, 0]).get_text()

        return Semester(date_semester=date_semester,
                        level_semester=level_semester,
                        observation=observation)

    try:
        for sem_index in range(14):
            html_sem = get_html_semester(sem_index)
            sem = semester_from_html(html_sem)
            semesters.append(sem)
            print(f"Scrapping Semester {sem.date_semester}")

            for type in type_course.keys():
                try:
                    for course_index in range(5):
                        html_course = html_course_of_semester(type, html_sem, course_index)
                        course_result = course_result_from_html(html_course, sem.date_semester)
                        print("> ", course_result)
                        course_results.append(course_result)
                except Exception:
                    pass
    except Exception:
        pass

    print("Done Scrapping Courses !")
    return semesters, course_results


def scrap_all():
    """
    In this order :
        1) Reset the data base
        2) Scraps and extracts the students CourseResults
        3) Scraps and extracts the associated Courses and CoursesDescription
        4) Insert everything in the database
    """
    # Cleaning things
    if input(f"Resetting the database may resolve problems ; do you want to do it? [Y/whatever]   ") == "Y":
        reset_db()

    # Getting Semesters and Results
    html_of_result = get_html_results(encoding="iso-8859-15")
    semesters, course_results = scrap_results(html_of_result)

    # Getting Info about courses
    courses_codes = list(map(lambda cr: cr.course_code, course_results))
    courses, courses_descriptions, missing_courses_code = \
        scrap_courses_description(lang=lang, courses_to_scrap=courses_codes)

    # Missing courses are courses present in the student profile but that are not on the
    missing_courses = list(map(lambda cc: Course(course_code=cc), missing_courses_code))

    # Inserting info in the database
    db_string = get_db_string()
    engine = create_engine(db_string, echo=False)
    session_maker = sessionmaker(bind=engine)

    for batch in [semesters, missing_courses, courses, courses_descriptions, course_results]:
        session = session_maker()
        session.add_all(batch)
        session.commit()


if __name__ == "__main__":
    scrap_all()
