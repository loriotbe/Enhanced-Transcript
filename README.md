Enhanced Transcript of Records 📖 ☑ 
====================

Generate a nice transcript of records automatically for you 👌

![e](./doc/output_extract.png)

## Setting up the project :

Here, we will be using `pipenv` to manage the environnement and packages. But you can use what you want.

 - Create a new virtual environement using `pipenv` :

```bash
cd handMadeTranscript/
pipenv --three
```

- Install the dependencies

```bash
pipenv install -r requirement.txt
```

- Download `chromedriver`and put it in the `venv ` `bin/`  virtuel. It is needed by `selenium` to scrap data. On Linux :

```bash
wget https://chromedriver.storage.googleapis.com/2.40/chromedriver_linux64.zip
unzip chromedriver_linux64.zip
mv chromedriver $(pipenv --venv)/bin
rm chromedriver_linux64.zip
```

- Activate the shell to use the virtual environnement :

```bash
pipenv shell
```

**Optional **: To compile your the final pdf in the row, you need to have `latexmk` installed but this is not required and you can then compile the final `.tex` yourself.  



## Usage

### As a student from the University of Technology of Compiègne 🍺

After having setting up the project (see above) :

- Scrap the data ; on the first time, it will ask to enter your credentials to get your data. All the data will be persisted in a local SQLite database (by default `data/homeMadeTranscript.db`)

```bash
python ./scrap.py
```

- Create the final `.tex` file and compile it using :s

```bash
python ./latexer.py
```

Annnnd you're done ! 🙃 

### As another student from another university in the 🗺

You have to get your course data yourself unfortunately.

Then, you can use the database schema defined in `models.py` to format it and then use the latexer to output your document : 

```bash
python ./latexer.py
```



**Note** : for now, this project is mostly done for an by UTC students ; we'll try to make it as general as possible in order for you — other world citizens — to use it.



## Project Structure

This project is organised in (simplistic) modules :

- `models.py` : defines the schema of the data to be persisted (see bellow)
- `scrap.py` : scraps data about your results and about the courses and push it in the data base (**for now, usable by UTC students only**)
- `latexer.py` : creates the final `.tex` file and compiles it
- `experiments.py` : experiments that you should not try at home ⚠
- `data` : persisted data (not synced with the `git` repo')
- `out` : final generated files and template files used to build it (final files not synced with the `git` repo)  
- `settings_and_misc` : settings (for `data` and `out` mainly) and various little functions to ease various things



## Data Base Schema

Data gets persisted in a local SQLite database using this simple schema. 

![UML](./doc/uml.png)

### Using your own database :

You can use your own data base (PostGRESQL, MySQL, WhateverSQL…) if you want ! To do this, just `echo` your `database_url` in the file `.credentials` ( `cat .credentials.dist` to see the format to use).

## About the template and this project license

To generate the final document, we used a modified version of [this template](https://itp.uni-frankfurt.de/~guterding/misc/Transcript.tex) from [muellis](https://blogs.gnome.org/muelli/) (see [here](https://blogs.gnome.org/muelli/2009/06/latex-transcript-of-records/)).

This project license is MIT.

